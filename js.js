let inputOne = document.querySelector('.input-one')
let iconOne = document.querySelector('.icon-password-one')
iconOne.classList.add('fa-eye-slash')

iconOne.addEventListener('click', ()=>{
    if (inputOne.getAttribute('type') === 'password'){
        inputOne.setAttribute('type','text');
        iconOne.classList.remove('fa-eye-slash')
    }else{
        inputOne.setAttribute('type', 'password');
        iconOne.classList.add('fa-eye-slash')
    }
})


let inputTwo = document.querySelector('.input-two')
let iconTwo = document.querySelector('.icon-password-two')

iconTwo.classList.add('fa-eye-slash')
iconTwo.addEventListener('click', ()=>{
    if (inputTwo.getAttribute('type') === 'password'){
        inputTwo.setAttribute('type','text');
        iconTwo.classList.remove('fa-eye-slash')
    }else{
        inputTwo.setAttribute('type', 'password');
        iconTwo.classList.add('fa-eye-slash')
    }
})

let btn = document.querySelector('.btn')
btn.addEventListener('click', ()=>{
    if (inputOne.value === inputTwo.value){
        alert('You are welcome')

    } else {
        let input2 = document.querySelector('.input-wrapper-two')
       input2.insertAdjacentHTML('beforeend', 'Потрібно ввести однакові значення');
        input2.style.color = 'red'
     
    }
})
